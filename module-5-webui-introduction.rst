.. role:: green

.. container:: duration
    
    🕑 1 h

.. raw:: html    

  <label for="file">Workshop Completion...</label>
  <progress id="file" value="60" max="100"> 60% </progress>    
  
Module 5 - WebUI introduction :green:`✓`
#########################################

Overview
-------------------------

In this tutorial, the goal is to build the minimal UI attached to our AIMMS project built in :doc:`module-4-build-the-model`. 
You will be introduced to the WebUI, and you will learn several ways to customize it. 

What you'll learn
++++++++++++++++++

:green:`✓`  WebUI basics (Launch, app settings, widget manager, pages)

:green:`✓`  The table widget, map widget, slider widget, widget settings (pivoting, slicing & display domain, store focus)

:green:`✓`  How to customize UI through CSS, annotations, set element text. 

:green:`✓`  The WebUI folder

:green:`✓`  The Page Action


What you'll need
++++++++++++++++++

- The Excel file to read :download:`resources/InitialData.xlsx`

- :doc:`module-4-build-the-model` done ! 😀

- If not completed, the final model of :doc:`module-4-build-the-model`. :download:`SW_Usecase_module4_final.7z <resources/SW_Usecase_module4_final.7z>`


WebUI basics
----------------

.. raw:: html

   <video controls poster="_static/videos/webuibasics.png" src="_static/videos/webuibasics.mp4" style="max-width:100%;"></video>

.. tip::
  
  - Best Practise: ``Maxcolumns`` = 10 in the Page settings
   
Table widget
------------------

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/tablewidget.png" src="_static/videos/tablewidget.mp4" style="max-width:100%;"></video>


Map widget
-------------

In order to display our solution on the map, we need to import locations coordinates first, included in the Excel File tab *Coordinates*.

By getting some inspiration from :doc:`module-3-excel-import`, *would you be able to import Latitude and Longitude coordinates?*

.. raw:: html

   <details style="border: solid 1px gray; border-radius: 5px;">
   <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

First, We need to declare 3 new parameters

.. code::
    
    Parameter P_Latitude {
      IndexDomain: l;
    }
    Parameter P_Longitude {
      IndexDomain: l;
    }
    Parameter P_Population {
      IndexDomain: l;
    }
    
And then add the following lines the ``ReadFromExcel`` procedure

.. code::
    
    
    axll::SelectSheet("Coordinates");

    axll::ReadList(P_Latitude,"A2:A200","B2:B200",ModeForUnknownElements :  1);
    axll::ReadList(P_Longitude,"A2:A200","C2:C200",ModeForUnknownElements :  1);
    axll::ReadList(P_Population,"A2:A200","D2:D200",ModeForUnknownElements :  1);


.. raw:: html
  
  </details>

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/mapwidget.png" src="_static/videos/mapwidget.mp4" style="max-width:100%;"></video>
  
  
Customize the UI
------------------

**CSS styling**

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/cssstyling.png" src="_static/videos/cssstyling.mp4" style="max-width:100%;"></video>

.. tip::
    
    - ``CTRL`` + ``SHIFT`` + ``I`` Inspect the page
    - ``<root>/MainProject/WebUI/resources`` folder
    - `More on CSS <https://www.w3schools.com/html/html_css.asp>`_
    - `More on CSS styling an AIMMS app <https://documentation.aimms.com/webui/css-styling.html>`_

**Identifier Names customization**
   
.. raw:: html

   <video controls preload="metadata" poster="_static/videos/propertiesfile.png" src="_static/videos/propertiesfile.mp4" style="max-width:100%;"></video>

.. tip::
   
    - ``<root>/MainProject/WebUI/resources`` folder 



Introducing a Page Action
----------------------------

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/pageaction.png" src="_static/videos/pageaction.mp4" style="max-width:100%;"></video>



.. seealso::
    
    - `Quick start a WebUI <https://documentation.aimms.com/webui/quick-start.html>`_

