.. role:: green

.. container:: duration
    
    🕑 5 min
  
  

Overview
===========

In this tutorial, the goal is to showcase and get a sense on the application we want to build in this tutorial series. 
We will quickly showcase the end model, fully built and operational on the AIMMS cloud.



Video Tutorial
----------------

.. raw:: html

   <video controls poster="_static/videos/2-Showcase.png" src="_static/videos/2-Showcase.mp4" style="max-width:100%;"></video>

Recap of our Problem
---------------------------

.. figure:: resources/model-challenge.svg
  :align: center


:green:`✓` 3 types of nodes
   - Factory
   - NDC (National Distribution Center)
   - RDC (Regional Distribution Center)

:green:`✓` **Our goal:** Minimize Transport Cost

:green:`✓` **Different cost** on arcs between Factory-NDC and NDC-RDC




