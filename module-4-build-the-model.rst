.. role:: green

.. container:: duration
    
    🕑 1 h 30 min

.. raw:: html    

  <label for="file">Workshop Completion...</label>
  <progress id="file" value="50" max="100"> 50% </progress>
  
Module 4 - Build the math model :green:`✓`
#############################################

Overview
-------------------------

In this tutorial, the goal is to formulate the transport model that was showcased, and implement it in AIMMS. 

As described in :doc:`module-2-endmodel`, this model is structured in 2 flows. 
The first flow from Factories to NDC (Primary Flow), and the second flows from NDC to RDC (Secondary Flow). 

Our first challenge is to write the mathematical formulation associated with this problem.
Our second challenge is to translate this formulation in AIMMS developer IDE.

What you'll learn
++++++++++++++++++

:green:`✓`  The AIMMS Domain condition, and how to apply it

:green:`✓`  How to formulate a "node equilibrium" constraint,

:green:`✓`  AIMMS Pivot table


What you'll need
++++++++++++++++++

- :doc:`module-1` reviewed

- :doc:`module-2-endmodel` reviewed

- :doc:`module-3-excel-import` done ! 😀

- If not completed, the final model of :doc:`module-3-excel-import`. :download:`SW_Usecase_module3_final.7z <resources/SW_Usecase_module3_final.7z>`

Video 1 - Introducing the AIMMS domain condition
-------------------------------------------------

.. raw:: html

   <video controls poster="_static/videos/TheDomainCondition.png" src="_static/videos/TheDomainCondition.mp4" style="max-width:100%;"></video>

.. tip::
  
  - ``CTRL`` + ``D`` shows the data
  - ``CTRL`` + ``SPACE`` 2 times auto-completes
  - The :any:`uniform` distribution
  - ``<index domain> | <logical condition>`` domain condition syntax

Step 2 - What is the mathematical formulation of our problem?
---------------------------------------------------------------

Herein below a small recap of our network design to implement

.. figure:: resources/model-challenge.svg

- By getting some inspiration from :doc:`module-1` , *What variable(s) should we introduce first?*

  .. raw:: html

     <details style="border: solid 1px gray; border-radius: 5px;">
     <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

  **The transport variable**   

  .. math::
    
     V_T(l,l2,t) \geq 0, \quad \forall (l,l2) \in S_{locations}^2 \textrm{ and } t \in S_{Time}

  **The production variable**   

  .. math::
    
     P_{Supply}(l) \geq V_{Production}(l,t) \geq 0, \quad \forall l \in S_{Locations} \textrm{ and } t \in S_{Time}
  
  .. note:: 

    Theoretically, we could have introduced only the transport variable. 
    However, as you will see later in the training, we can't be sure to have an exact equality between supply and demand. Not using this production variable could possibly lead us to an infeasibility.
       
  .. raw:: html

    </details>

  It is the correct symbolic variables to introduce, however we might not need all the variables (arcs) defined in the matrix. 
  Mathematically speaking, we could go on, and let this variable as general as it is. 
  However, we do need to take care of computation, as we are using a computer to solve this problem.
  This matrix represents a large number of arcs, since we are considering every arc between every location to every other location. 
  If we would import another dataset with much more locations, we would generate a huge number of arcs (:math:`n^2`), while we know we will use just a few of them.

- *How could we slice this symbolic variable, in order to generate just the arcs we need?*

  .. raw:: html

     <details style="border: solid 1px gray; border-radius: 5px;">
     <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

  We may use the information contained in the ``P_LocationType(l,typ)``. 
  Together with the domain condition introduced above, we will slice ``V_T`` to generate only allowed variables (arcs) for the primary flow, and the secondary flow.

  .. math::

       V_T(l,l2) \geq 0, \quad \forall (l,l2,t) &\in S_{Locations}^2 \textrm{ such that } P_{DomainCondition}(l,l2) \neq 0
       
       &  
       
       & \textrm{Knowing that}


  .. math::

         P_{DomainCondition}(l,l2) =
           \begin{cases}
             1,& \textrm{if tuple} (l,l2) \textrm{in primary flow or secondary flow} \\
             0,& \textrm{otherwise} \\
           \end{cases}


  Finally, the logical condition contained in the domain condition would be
    
  .. math::
  
      &P_{DomainCondition}(l,l2) = &P_{LocationType}(l,Factory) \quad & and \quad P_{LocationType}(l2,NDC)
      
      &&or
      
      &&P_{LocationType}(l,NDC) \quad & and \quad P_{LocationType}(l2, RDC)
      
      
  .. raw:: html

     </details>
  
  Now that our symbolic variables are tightly defined, we may introduce our constraints. 
  Thanks to this tight variable formulation, it is possible to introduce a **unique** symbolic constraint in AIMMS. Would you be able to find its definition? 

- By getting some inspiration from :doc:`module-1` , *What constraints should we introduce?*

  .. raw:: html
 
       <details style="border: solid 1px gray; border-radius: 5px;">
       <summary style="cursor: pointer;">Click me to see the answer 👇</summary>
  
  We may introduce the **Node Equilibrium constraint**, where nodes are locations. There is an equilibrium between quantity getting in the node, and quantity getting out of the node.
  
  .. math::
    
    inbound &= outbound
    
    \\
    
    \forall (l,t), \quad V_{Production}(l,t) + \sum_{l2} V_T(l2,l,t) &= \sum_{l2} V_T(l,l2,t) \quad + P_{Demand}(l,t)
  
  The equivalent AIMMS implementation would be:
  
  
  .. raw:: html

     </details>  

- Finally, *What objective function should we introduce?*

  .. raw:: html
 
       <details style="border: solid 1px gray; border-radius: 5px;">
       <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

  .. math::
    
    \sum_{l,l2,t} V_T(l,l2,t)*P_{DistanceBetweenLocation}(l,l2)*P_{UnitTransportCost}(l,l2)
  
  
  .. raw:: html

     </details>  

    

Video 3 - Building the model in AIMMS IDE
------------------------------------------

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/model_building_part1.png" src="_static/videos/model_building_part1.mp4" style="max-width:100%;"></video>

.. tip::
  
  - ``CTRL`` + ``D`` shows the data
  - ``CTRL`` + ``SPACE`` 2 times auto-completes
  - ``RIGHT CLICK`` + ``Run Procedure...`` to run the main execution



Video 5 - Analysing the results
--------------------------------------

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/pivottable.png" src="_static/videos/pivottable.mp4" style="max-width:100%;"></video>


Notes
-------

https://en.wikipedia.org/wiki/Minimum-cost_flow_problem

