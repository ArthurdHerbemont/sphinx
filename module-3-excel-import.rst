.. role:: green

.. container:: duration
    
    🕑 45 min

.. raw:: html    

  <label for="file">Workshop Completion...</label>
  <progress id="file" value="25" max="100"> 25% </progress>    
  
Module 3 - Import Excel Data :green:`✓`
########################################

Overview
-------------------------

In this tutorial, the goal is to import an Excel file into an AIMMS model, from scratch. 
You will implement the identifier structure as well as the procedure to import the Excel file. 
You will finally import the data into your AIMMS model.

What you'll learn
++++++++++++++++++

:green:`✓`  Import a section, and what is an ``AMS`` file

:green:`✓`  Import a library,

:green:`✓`  Find a correct indexing out of your dataset,

:green:`✓`  Use the ``AIMMSXLLibrary``,


What you'll need
++++++++++++++++++

- The Excel file to read :download:`resources/InitialData.xlsx`

- The AIMMS section to import :download:`resources/ExcelReadingSection.ams`

- :doc:`module-1` done ! 😀

Video 1 - Description of the Excel file to import
-------------------------------------------------
.. raw:: html

   <video controls poster="_static/videos/Description_ExcelFile.png" src="_static/videos/Description_ExcelFile.mp4" style="max-width:100%;"></video>


Video 2 - Import the AIMMSXLLibrary
--------------------------------------

.. raw:: html

   <video controls poster="_static/videos/Description_ExcelFile.png" preload="metadata" src="_static/videos/import-aimmsxllibrary.mp4" style="max-width:100%;"></video>

.. tip::

  - *Edit → Import...* imports an ``AMS`` file. In our case, a section. 
  - *File → Library Manager... → Add System Libraries... →* ``AIMMSXLLibrary`` will import the ``AIMMSXLLibrary``


Video 3 - Define the parameter placeholders and import the Excel Data
---------------------------------------------------------------------------

Now that the AIMMSXLLibrary is imported, we can see in the ``ReadFromExcel`` procedure a few parameters still to define (in red, not recognised by AIMMS IDE).
Each of those parameter (``P_LocationType``, ``P_DistanceBetweenLocations`` , ...) are placeholders for the data contained in each tab of the Excel file. However, those parameters are not defined yet, and their indexing is not known yet. 

What identifiers should we introduce in our model ?

*What is the set structure we should define to import this Excel file ?*

.. raw:: html

   <details>
   <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

 
.. code-block:: aimms

    Set S_Locations {
        Index: l,l2;
    }
    Set S_LocationTypes {
        Index: typ;
    }
    Set S_Time {
        Index: d;
    }

.. raw:: html

   </details>

*Consequently, what are parameter declarations?*

.. raw:: html

   <details>
   <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

.. code-block:: aimms

  Parameter P_LocationType {
    IndexDomain: (l,typ);
  }
  Parameter P_DistanceBetweenLocations {
    IndexDomain: (l,l2);
  }
  Parameter P_Demand {
    IndexDomain: (l,d);
  }
  Parameter P_Supply {
    IndexDomain: (l);
  }
  Parameter P_MinStock {
    IndexDomain: (l);
  }
  Parameter P_MaxStock {
    IndexDomain: (l);
  }
  Parameter P_StartStock {
    IndexDomain: (l);
  }

.. raw:: html

   </details>
   
**Explanations**

.. raw:: html

   <video controls poster="_static/videos/Description_ExcelFile.png" preload="metadata" src="_static/videos/import_excel_data.mp4" style="max-width:100%;"></video>


.. note:: 
  
  When executing the ``ReadFromExcel`` procedure, some warnings will be raised, in the *Errors/Warnings* window. Those are expected, since ranges defined in the different reading procedures (``ReadList``, ``ReadTable``) are purposely too large. 
  Those warnings can be avoided by modifying the ``ModeForUnknownElement`` argument, as specified in the documentation contained in the comments of the ``AIMMSXLLibrary``.

Video 4 - Project start up configuration
---------------------------------------------

.. raw:: html

   <video controls poster="_static/videos/projectstartup.png" preload="metadata" src="_static/videos/projectstartup.mp4" style="max-width:100%;"></video>

.. tip::
    
    - *Settings → Project Options... → Project → Startup & authorization → Startup case*
    - *Settings → Project Options... → Project → Startup & authorization → Startup procedure*
    - ``PostMainInitialisation`` procedure

    
Congrats !
--------------------------

You are all set for :doc:`module-4-build-the-model`

You can click on the "next" button at the bottom right of this page
