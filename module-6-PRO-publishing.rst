.. role:: green

.. container:: duration
    
    🕑 30 min
  
.. raw:: html    

  <label for="file">Workshop Completion...</label>
  <progress id="file" value="70" max="100"> 60% </progress>    
 
 
Module 6 - PRO publishing :green:`✓`
######################################

Overview
-------------------------

In this tutorial, the goal is to publish our current project on the PRO platform, and to make it available for others.

We will also learn a bit more about the AIMMS PRO platform.

What you'll learn
++++++++++++++++++

:green:`✓`  To prepare your model for publishing

:green:`✓`  To publish an AIMMS model on PRO

:green:`✓`  To manage user permission

:green:`✓`  To access and understand the Job overview


What you'll need
++++++++++++++++++

- If not completed, the final model of :doc:`module-5-webui-introduction`. :download:`SW_Usecase_module5_final.7z <resources/SW_Usecase_module5_final.7z>`
- a PRO or AIMMS Cloud server available 
- an account with a right to "publish" models

Prepare and publish
---------------------

.. raw:: html

   <video controls poster="_static/videos/propublishing.png" src="_static/videos/propublishing.mp4" style="max-width:100%;"></video>

.. tip::
  
  - .. code::
    
      if not ProjectDeveloperMode then
        if pro::DelegateToServer(
              completionCallback: 'pro::session::LoadResultsCallback',
              waitForCompletion:1) then
          return 1;
        endif ;
      endif;

  - To export an ``.aimmspack``: *File → Export End User Project...*

.. seealso::
  
  - Documentation about ``pro::DelegateToServer`` arguments: http://documentation.aimms.com/pro/pro-delegate-adv.html
  - More about `advanced PRO publishing <https://how-to.aimms.com/C_Deployment/Sub_Client_Server/index.html>`_
  - `Conversion guidelines to publish your model <https://documentation.aimms.com/pro/conversion-to-pro.html>`_

.. warning::
  
  -  To show the "*Publish app*" button, your account needs to be part of the ``AppPublishers`` group from the ``ROOT`` environnement of your PRO platform. Please ask your PRO administrator for further details.
 
User Management
-------------------

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/permissions.png" src="_static/videos/permissions.mp4" style="max-width:100%;"></video>

 
 
.. tip::

  - `User Permissions documentation <https://documentation.aimms.com/pro/appl-man.html#different-rights>`_
 
Job Overview
---------------
 
.. raw:: html

   <video controls preload="metadata" poster="_static/videos/jobs.png" src="_static/videos/jobs.mp4" style="max-width:100%;"></video>

Notes
---------

.. note::
  
  Using the AIMMS Cloud platform and `AIMMS Version 4.57 <https://documentation.aimms.com/release-notes.html#aimms-4-57>`_, 
  you may use the `Solver lease <https://documentation.aimms.com/pro/solver-lease.html>`_ to solve small optimization models faster. 
  
    
