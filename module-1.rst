.. role:: green

.. .. container:: duration
    
..     🕑 1h

.. Module 1 - Tutorial for beginners
.. =====================================

.. container:: duration
    
    Approx read time: 1h

Overview
=============

As a beginner into optimization modeling languages, you may not have
much time for learning yet another tool in order to finish some project
or homework requirements. In this case, concentrate your efforts on this
tutorial. After completing this tutorial, you should be able to use the
system to build your own simple models, and to enter your own small data
sets for subsequent viewing. The book on `Optimization Modeling <https://manual.aimms.com/_downloads/AIMMS_modeling.pdf>`_ may
teach you some useful tricks, and will show you different (mostly
non-trivial) examples of optimization models.

What you'll learn
++++++++++++++++++

Once you have read the short `problem description <#Problem-description-and-model-statement>`_ and the associated
mathematical model statement, you will be asked to complete a series of
tasks that make up this one-hour tutorial, namely:

:green:`✓`  create a new project in AIMMS,

:green:`✓`  enter all identifier declarations,

:green:`✓`  enter the data manually,

:green:`✓`  save your data in a case,

:green:`✓`  build a small procedure,


What you'll need
++++++++++++++++++

- `AIMMS developer IDE <http://aimms.com/english/developers/downloads/download-aimms/>`_

- Some basics on mathematical formulations and Optimization mathematical models 


Video Tutorial
----------------

We found the following 16 steps beginners tutorials by Álvaro García Sánchez extremely helpful.

   
.. raw:: html
    
    <div style="max-width=100%">
    <iframe src="https://www.youtube.com/embed/videoseries?list=PLZqh3oAyX6qlV23t7c-McVJA-rHQHjnKD" frameborder="0" style="width: -webkit-fill-available;height: -webkit-fill-available;" allowfullscreen></iframe>
    </div>
   



Problem description and model statement
---------------------------------------

.. rubric:: Problem description

Truckloads of beer are to be shipped from two plants to five customers
during a particular period of time. Both the available supply at each
plant and the required demand by each customer (measured in terms of
truckloads) are known. The cost associated with moving one truck load
from a plant to a customer is also provided. The objective is to make a
least-cost plan for moving the beer such that the demand is met and
shipments do not exceed the available supply from each brewery.

.. rubric:: Data overview

The following table provides the data for the problem described in the
previous paragraph.

.. _tb:tut:input_data:

.. table:: Input data for beer transport problem

  +----------------+----------------------------------------------------+--------+
  |      Customers |             Unit Transport Cost                    | Supply |
  |                +-----------+-------+-------+------------+-----------+        |
  | Plants         | Amsterdam | Breda | Gouda | Amersfoort | Den Bosch |        |
  +----------------+-----------+-------+-------+------------+-----------+--------+
  | Haarlem        | 131       | 405   | 188   | 396        | 485       | 47     |
  +----------------+-----------+-------+-------+------------+-----------+--------+
  | Eindhoven      | 554       | 351   | 479   | 366        | 155       | 63     |
  +----------------+-----------+-------+-------+------------+-----------+--------+
  | Demand         | 28        | 16    | 22    | 31         | 12        |        |
  +----------------+-----------+-------+-------+------------+-----------+--------+


.. rubric:: Identifier declarations

The following declarations list the identifiers that are part of the
mathematical program to be built.

.. math::

   \begin{align}
   & \textbf{Indices:} \\
   &&& \text{$p$} & & \text{plants} \\
   &&& \text{$c$} & & \text{customers} \\[0.5pc]
   & \textbf{Parameters:} \\
   &&& \text{$S_p$} & & \text{supply at plant $p$} \\
   &&& \text{$D_c$} & & \text{demand by customer $c$}  \\
   &&& \text{$U_{pc}$} & & \text{unit transport cost from $p$ to $c$} \\[0.5pc]
   & \textbf{Variables:} \\
   &&& \text{$x_{pc}$} & & \text{transport from $p$ to $c$} \\
   &&& \text{$z$} & & \text{total transport cost} \\[0.5pc]
   \end{align}

.. rubric:: Model summary

The mathematical model summary below captures the least-cost plan to
transport beer such that the demand is met and shipments do not exceed
available supply.

.. math::

   \begin{align}
   z = \sum_{pc} U_{pc} x_{pc} &  & &  \\
   \end{align}

.. math::

   \begin{align}
   \sum_c x_{pc} & \leq S_p & & \forall p \\
   \sum_p x_{pc} & \geq D_c & & \forall c \\
   x_{pc} & \geq 0 & & \forall (p,c) \\
   \end{align}

.. figure:: figs/netherlands.png
   :name: fg:tut:nederland
   :align: center

   The Netherlands
   
.. rubric:: Using explicit names

Even though the above notation with one-letter symbols is typical of
small mathematical optimization models, it will not be used to represent
the model in AIMMS. Instead, explicit names will be used throughout to
avoid any unnecessary translation symbols. The number of symbols needed
to describe practical applications is generally large, and a clear
naming convention supports the understanding and maintenance of large
models.   