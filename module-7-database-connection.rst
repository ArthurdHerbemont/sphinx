.. role:: green

.. container:: duration
    
    🕑 45 min
  
.. raw:: html    

  <label for="file">Workshop Completion...</label>
  <progress id="file" value="75" max="100"> 75% </progress>    
   
Module 7 - Database Connection :green:`✓`
#############################################

Overview
-------------------------

In this tutorial, the goal is to connect with a database of cities in the world, and import European data in our model.

What you'll learn
++++++++++++++++++

:green:`✓`  To use the database identifier 

:green:`✓`  To connect with a database

:green:`✓`  To implement and use an SQL query,

What you'll need
++++++++++++++++++

- The SQLite database file to read :download:`resources/Cities.db`
- An SQLite database driver installed on your computer, as http://www.ch-werner.de/sqliteodbc/
- If not completed, :download:`SW_Usecase_module5_final.7z <resources/SW_Usecase_module5_final.7z>` or :download:`SW_Usecase_module6_final.7z <resources/SW_Usecase_module6_final.7z>`
- (Optional) https://sqlitebrowser.org/ to read the content of the database file outside of AIMMS

Using the database identifier
-------------------------------

.. raw:: html

   <video controls poster="_static/videos/databaseidentifier.png" src="_static/videos/databaseidentifier.mp4" style="max-width:100%;"></video>

.. tip::

  - https://sqlitebrowser.org/
  - .. code:: 
  
      "DRIVER=SQLite3 ODBC Driver;Database=Cities.db;"
    
    More on `connection strings <https://www.connectionstrings.com/sqlite/>`_
  - Overall steps to interact with a database in AIMMS:
      #. Create a database identifier
      #. Connect to a datasource
      #. Connect to a table
      #. Implement the mapping (AIMMS identifiers ⇔ Database columns)
      #. Read/write (with a procedure)

.. _using-an-SQL-query:

Using an SQL Query 
--------------------

Our challenge is to import a European dataset. 
This dataset should contain cities with more than 300.000 people, located such that :math:`40 < Latitude < 70` and :math:`-20 < Longitude < 20`. 

These constraints should cover the following area:

.. image:: figs/DatabaseArea.png

.. raw:: html

   <video controls preload="metadata" poster="_static/videos/sql.png" src="_static/videos/sql.mp4" style="max-width:100%;"></video>

.. tip::
  
  - .. code:: 
      
      "SELECT * FROM `simplemaps-worldcities-basic` WHERE (pop > 300000 and lat > 40 and lat < 70 and lng > -20 and lng < 20)"
 
    More on `SQL Queries <https://www.w3schools.com/sql/>`_
  

.. note::
    
    The SQL query is a string, thus you can dynamically build it thanks to string parameters. For example:
    
    .. code-block:: aimms
        
        "SELECT * FROM `simplemaps-worldcities-basic` WHERE (pop > " + P_MinPopulation + " and lat > " + P_MinLatitude + " and lat < " + P_MaxLatitude + " and lng > " + P_MinLongitude + " and lng < " + P_MaxLongitude + ")"
    
Notes
---------

- AIMMS is using the `ODBC layer <https://en.wikipedia.org/wiki/Open_Database_Connectivity>`_ of your windows computer. 
  Thus, **any ODBC compliant database is supposedly compatible with AIMMS**. We regularly deal with SQL Server, MySQL, Oracle databases for example.
  
- `Language Reference - Communicating With Databases <https://download.aimms.com/aimms/download/manuals/AIMMS3LR_CommunicatingWithDatabases.pdf>`_
  
- Final model :download:`SW_Usecase_module7_final.7z <resources/SW_Usecase_module7_final.7z>`
