Overview of the Standard Workshop
####################################

Here is an AIMMS website to attempt an on-line version of the AIMMS Standard workshop, to learn the basics of AIMMS, in our opinion.

   
.. toctree::
   :maxdepth: 1

   module-1
   module-2-endmodel
   module-3-excel-import
   module-4-build-the-model
   module-5-webui-introduction
   module-6-PRO-publishing
   module-7-database-connection
   module-8-aimms-language
.. .. rubric:: Indices and tables

.. * :ref:`genindex`
.. * :ref:`search`
   
   