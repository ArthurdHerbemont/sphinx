# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from sphinx.writers.html import HTMLTranslator
from docutils import nodes
from docutils.nodes import Element
import pdb
from better import better_theme_path

# import sphinx_docs_theme
# import cloud_sptheme
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = u'AIMMS Academy'
copyright = u'2020, AIMMS User Support'
author = u'AIMMS User Support'

book_title = ""

# The short X.Y version
version = u''
# The full version, including alpha/beta/rc tags
release = u''

# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
    'sphinx.ext.intersphinx',
	  'sphinx.builders.linkcheck',
    'sphinx_aimms_theme'
]

if os.name != 'nt':

    #Imports sitemap extension to build the sitemap automatically
    extensions.append('sphinx_sitemap')
    html_baseurl = "https://how-to.aimms.com/tutorials/"
    extensions.append('sphinx_last_updated_by_git')
    
intersphinx_mapping = {'aimmsdoc': ('https://documentation.aimms.com',
                                  (None,'objects-docs.inv')),
                       'functionreference': ('https://documentation.aimms.com/functionreference/',
                                  (None,'objects-functionreference.inv'))}
 

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path .
exclude_patterns = [u'_build', 'Thumbs.db', '.DS_Store']

rst_prolog = """
:tocdepth: 2
"""

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

pygments_style = 'default'
#html_theme = 'sphinx_aimms_theme'
html_theme_path = [better_theme_path]
html_theme = 'better'
# html_theme_path = sphinx_docs_theme.get_html_theme_path()
# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
if os.name == 'nt':
   
   Display_edit_on_gitlab = True
   # if builds locally (a windows machine), do not displays external extensions (Google Analytics, Community Embeddable, Algolia search, etc.)
   Display_3rd_Party_Extensions = False
else:

   # if builds on GitLab (a Linux machine), force "Edit on Gitlab" not to be shown, and displays external extensions (Google Analytics, Community Embeddable, Algolia search, etc.)
   Display_edit_on_gitlab = False
   Display_3rd_Party_Extensions = True
   
html_theme_options = {

    #'home_page': 'index',
    # 'doc_title' : 'Standard Workshop Online',
    # 'home_page_title': 'Standard Workshop Online',
    # 'home_page_description': "AIMMS Standard Workshop is a website to get you started with AIMMS",
    'display_community_embeddable' : False,
    'display_community_help_link' : False,
    'google_analytics_id': 'UA-1290545-13',
    'generate_google_analytics' : False,
    'display_algolia_search' : False,
    'algolia_appid': 'BH4D9OD16A', 
    'algolia_appkey': 'f7e44f5b57ababa5c5ceb1e1087ae3b1', 
    'algolia_indexname': 'aimms_how-to',
    'display_local_toc' : False,
    'nosidebar': True,
    'breadcrumbs': False,
    'show_insipid': False,
    'prev_next_buttons_location': None,
    'showheader': False,
    'showrelbartop': False,
  # same for bottom of the page
    'showrelbarbottom': False,
    'linktotheme': False,
    'footertextcolor': '#FFFFFF'
}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_context = {
     'css_files': ['_static/css.css' ] #'_static/custom.css'],
 }

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}

html_sidebars = {
    '**': ['globaltoc.html', 'searchbox.html']
}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'book-testdoc'


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    'pointsize': '11pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',
    
    #changes color of internal hyperlinks
    'passoptionstopackages': r'\PassOptionsToPackage{svgnames}{xcolor}',
    'sphinxsetup': 'InnerLinkColor={RGB}{203,65,84}',
    
    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'AIMMSXLLibrary_Documentation.tex', book_title,
     u'AIMMS', 'manual', True),
]

latex_logo = "AIMMS_logo_BlackWhite-PRINT-900x508.jpg"

latex_show_pagerefs = True

latex_show_urls = 'footnote'

latex_additional_files = ['_fortex/Makefile']

# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'master', book_title,
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'book-test', book_title,
     author, 'book-test', 'One line description of project.',
     'Miscellaneous'),
]


# -- Extension configuration -------------------------------------------------

add_function_parentheses = True

highlight_language = 'aimms'

class PatchedHTMLTranslator(HTMLTranslator):

    def visit_reference(self, node: Element) -> None:
        atts = {'class': 'reference'}
        if node.get('internal') or 'refuri' not in node:
            atts['class'] += ' internal'
        else:
            atts['class'] += ' external'
            # ---------------------------------------------------------
            # Customize behavior (open in new tab, secure linking site)
            atts['target'] = '_blank'
            atts['rel'] = 'noopener noreferrer'
            # ---------------------------------------------------------
        if 'refuri' in node:
            atts['href'] = node['refuri'] or '#'
            if self.settings.cloak_email_addresses and atts['href'].startswith('mailto:'):
                atts['href'] = self.cloak_mailto(atts['href'])
                self.in_mailto = True
        else:
            assert 'refid' in node, \
                   'References must have "refuri" or "refid" attribute.'
            atts['href'] = '#' + node['refid']
        if not isinstance(node.parent, nodes.TextElement):
            assert len(node) == 1 and isinstance(node[0], nodes.image)
            atts['class'] += ' image-reference'
        if 'reftitle' in node:
            atts['title'] = node['reftitle']
        if 'target' in node:
            atts['target'] = node['target']
        self.body.append(self.starttag(node, 'a', '', **atts))
 
        if node.get('secnumber'):
            self.body.append(('%s' + self.secnumber_suffix) %
                             '.'.join(map(str, node['secnumber'])))

def setup(app):
    app.set_translator('html', PatchedHTMLTranslator)
    app.add_js_file('https://cdn.jsdelivr.net/npm/details-polyfill@1/index.min.js')

# def setup(app):
  
#   # For <details> HTML tags to compatible with Edge and IE 
#   app.add_js_file('https://cdn.jsdelivr.net/npm/details-polyfill@1/index.min.js')

