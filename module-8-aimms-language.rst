.. role:: green

.. container:: duration
    
    🕑 1 h 30 min
  
.. raw:: html    

  <label for="file">Workshop Completion...</label>
  <progress id="file" value="85" max="100"> 80% </progress>    
 
  
Module 8 - Using AIMMS language :green:`✓`
###########################################

Overview
-------------------------

In this tutorial, our goal is to automatically generate some initial data for our network optimization problem, using cities and corresponding population. 
First we randomly choose our Factories and NDCs over the set of imported locations, then we generate the demand and supply thanks to the corresponding population data.
We will get more familiar with AIMMS language and AIMMS constructs.

What you'll learn
++++++++++++++++++

:green:`✓`  The element parameter, the subset

:green:`✓`  The debugger 

What you'll need
++++++++++++++++++

- :doc:`module-7-database-connection` done ! 😀
- :download:`SW_Usecase_module7_final.7z <resources/SW_Usecase_module7_final.7z>`

1 - Randomly select our Factories and NDCs
---------------------------------------------

:ref:`using-an-SQL-query`, we imported a European data set. 

*How to write a procedure* ``PR_split_factory_ndc`` *that would randomly choose* **5** *factories and* **3** *NDCs out of the imported data set ?*

.. tip::
  
  You may use the following AIMMS objects and constructs:
  
  - `Option seed <https://how-to.aimms.com/Articles/119/119-Reproducible-Random-Sequence.html>`_
  - :any:`uniform`, :any:`round`, :any:`Element`, :any:`Card`
  - ``while...endwhile`` syntax in the `Language reference <https://documentation.aimms.com/_downloads/AIMMS_ref.pdf>`_
  - ``EP_Location``, an `element parameter <https://how-to.aimms.com/Articles/121/121-set-index-element-parameter.html#:~:text=It%20must%20be%20a%20set,the%20set%20of%20real%20numbers.>`_ over the set of ``Locations``.
  - *Tools → Diagnostic Tools → Debugger* enables the debugger mode to go through your procedure step by step 

.. raw:: html

     <details>
     <summary style="cursor: pointer;">Click me to see another tip 👇</summary>  
     
.. tip::  

  You may structure your procedure in the following manner:
  
  #. Database link
  #. Initialisation - *We first initialize all our locations to be RDCs*
  #. Factories selection - *We then choose 5 factories out of our entire set of locations, making sure it can’t be an RDC anymore.*
  #. NDCs selection - *And we finally do the same with NDCs!*
  
.. raw:: html

   </details>  

|

.. raw:: html

     <details style="border: solid 1px gray; border-radius: 5px;">
     <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

.. code::
    
    Procedure PR_split_factory_ndc {
      Body: {
          ! Import database and empty initial data
          Database_Import;

          ! Initialize no factory
          P_LocationType(l, 'Factory') := 0;

          ! The rest of the locations should be all RDCs
          P_LocationType(l, 'RDC') := 1;
          
          Option seed = 35; ! You can freely pick any number here

          ! choose randomly 5 factories
          while sum(l, P_LocationType(l, 'Factory')) < 5 do
                  EP_Location := Element( S_Locations, round(uniform(0.5,card(S_Locations)+0.5)));
                  P_LocationType(EP_Location, 'Factory') :=1;
                  P_LocationType(EP_Location, 'RDC') := 0;
          endwhile;

          ! choose randomly 3 NDCs
          while sum(l, P_LocationType(l, 'NDC')) < 3 do
                  EP_Location := Element( S_Locations, round(uniform(0.5,card(S_Locations)+0.5)));
                  P_LocationType(EP_Location, 'NDC') :=1;
          endwhile;
      }
      ElementParameter EP_Location {
        Range: S_Locations;
      }
    }
    

.. raw:: html

   </details>  
   
*How can we make sure a Factory will not be an NDC using our procedure ?* 

.. raw:: html

     <details style="border: solid 1px gray; border-radius: 5px;">
     <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

.. code-block:: aimms
    :linenos:
    :emphasize-lines: 1-3,16,17,21,24,29,31
    
    Set S_subLocations {
      SubsetOf: S_Locations
    }
    Procedure PR_split_factory_ndc {
      Body: {
          ! Import database and empty initial data
          Database_Import;

          ! Initialize no factory
          P_LocationType(l, 'Factory') := 0;

          ! The rest of the locations should be all RDCs
          P_LocationType(l, 'RDC') := 1;
          
          Option seed = 35; ! You can freely pick any number here
          
          !initialize subset of locations
          S_subLocations := S_Locations
          
          ! choose randomly 5 factories
          while sum(l, P_LocationType(l, 'Factory')) < 5 do
                  EP_Location := Element( S_subLocations, round(uniform(0.5,card(S_Locations)+0.5)));
                  P_LocationType(EP_Location, 'Factory') :=1;
                  P_LocationType(EP_Location, 'RDC') := 0;
                  S_subLocations -= EP_Location;
          endwhile;

          ! choose randomly 3 NDCs
          while sum(l, P_LocationType(l, 'NDC')) < 3 do
                  EP_Location := Element( S_subLocations, round(uniform(0.5,card(S_Locations)+0.5)));
                  P_LocationType(EP_Location, 'NDC') :=1;
                  S_subLocations -= EP_Location;
          endwhile;
      }
      
      ElementParameter EP_Location {
        Range: S_Locations;
      }
    }     
     
.. note:: 
  
  - As in many programming languages, a local identifier (like ``EP_Location`` in our case) is not accessible outside of the procedure, and its data will exist only at runtime.
   
2 - Generate Demand, Supply and Distances
---------------------------------------------

We should make sure of the following constraints are met.

#. Our demand should be proportional to the population, but with a small random noise.
#. Our supply should always meet our previously generated demand to not produce a infeasible model. Thus, we divide the total demand by the number of factories multiplied by the number of time slots (days).
#. Distances between location are flying distances, computed thanks to latitude and longitude coordinates
#. Finally, our Max Stock level is computed to buffer any outliers produced by demand generation, representing 10% of an average demand

*How to write a procedure* ``PR_generate_demand_supply_distances`` *to fill our* ``P_demand``, ``P_Supply``, ``P_DistancesBetweenLocations`` *?*

.. tip::
  
  - The ``empty`` statement and the ``mean`` function are documented in the `Language reference <https://documentation.aimms.com/_downloads/AIMMS_ref.pdf>`_
  - The flying distance between 2 points on a sphere can be computed by the `Haversine formula <https://en.wikipedia.org/wiki/Haversine_formula>`_
  
    .. code::
        
      sqrt(((P_Latitude(l) - P_Latitude(OtherL)) * 111.320 * cos(radians(1/2 * (P_Latitude(l) + P_Latitude(OtherL)))))^2 + ((P_Latitude(l) - P_Latitude(OtherL)) * 110.574 )^2) *1
  
.. raw:: html

     <details style="border: solid 1px gray; border-radius: 5px;">
     <summary style="cursor: pointer;">Click me to see the answer 👇</summary>

.. code::
    
    Procedure PR_Generate_Demand_Supply_Distances {
        Body: {
          P_Scale := 1/1000;
          Empty P_Demand, P_Supply;
          S_Time := {'Monday'}; !'Tuesday', 'Wednesday'};

          P_Demand(l, t) := round(P_Population(l) *(1 + uniform(-0.1,0.1))*P_Scale);

          P_Supply(l) | P_LocationType(l, 'Factory') := sum((l2), max(t,sum((l2),P_Demand(l2, t))) / (sum(l2, P_LocationType(l2, 'Factory'))*card(S_Time));

          P_DistanceBetweenLocations(l, l2) := sqrt(((P_Latitude(l) - P_Latitude(l2)) * 111.320 * cos(radians(1/2 * (P_Latitude(l) + P_Latitude(l2)))))^2 + ((P_Latitude(l) - P_Latitude(l2)) * 110.574 )^2) *1;

        }
        Parameter P_Scale;
    }
    

.. raw:: html

   </details> 
   
3 - Run the model
---------------------------------------------

Finally, run the ``MainExecution`` to solve the model, and see your results in the WebUI.

.. image:: figs/endmodel.png

.. raw:: html    

  <label for="file">Done ! 👏</label>
  <progress id="file" value="100" max="100"> 80% </progress> 


Congrats ! 😀 You have finished the standard workshop.

   

